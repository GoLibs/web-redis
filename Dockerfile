FROM gliderlabs/alpine:3.3
ADD ./hello /hello
EXPOSE 80
CMD ["/hello"]
