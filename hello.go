package main

import (
	"fmt"
    "net/http"
    "os"
    "github.com/garyburd/redigo/redis"
)

var (
	redis_host = os.Getenv("REDIS_PORT_6379_TCP_ADDR")
	redis_port = os.Getenv("REDIS_PORT_6379_TCP_PORT")
)

func helloHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := redis.Dial("tcp", redis_host + ":" + redis_port)
	defer conn.Close()
	if err != nil {
		fmt.Fprintln(w, fmt.Sprintln("connect", redis_host, "error port ", redis_port))
		return
	}
	hits, err := redis.Int(conn.Do("GET", "hits"))
	conn.Do("INCR", "hits")
    fmt.Fprintln(w, fmt.Sprintln("Hello World! I have been seen " , hits, "times."))
}

func main() {
	http.HandleFunc("/", helloHandler)

	fmt.Println("Started, serving at 80")
	err := http.ListenAndServe(":80", nil)
	if err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}
